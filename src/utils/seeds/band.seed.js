const mongoose = require('mongoose');
const db = require('../database/database');
const Band = require('../../api/bands/bands.model');

const initialBands = [
    {
        name: "Belako",
        albums: ["Eurie", "Hamen", "Render Me Numb", "Plastic Drama"],
        members: ["Josu Billelabeitia", "Cris Lizarraga", "Lander Zalakain", "Lore Billelabeitia"],
        yearsActive: "2011 - present"
    },
    {
        name: "León Benavente",
        albums: ["León Benavente", "2", "Vamos a Volvernos Locos", "Era"],
        members: ["Abraham Boba", "Eduardo Baos", "Luis Rodríguez", "César Verdú"],
        yearsActive: "2012 - present"
    },
    {
        name: "Mucho",
        albums: ["Mucho", "El Apocalipsis Segun Mucho", "Pidiendo en las Puertas del Infierno", "¿Hay Alguien en Casa?"],
        members: ["Martí Perarnau"],
        yearsActive: "2011 - present"
    },
    {
        name: "La Maravillosa Orquesta del Alcohol",
        albums: ["The Shape of Folk to Come","¿Quién Nos Va a Salvar?", "La Primavera del Invierno", "Salvavida (de las Balas Perdidas)", "&", "Ninguna Ola", "Nuevo Cancionero Burgalés"],
        members: ["Alvar de Pablo", "Caleb Melguizo", "Joselito Maravillas", "Jorge Juan Mariscal", "David Ruiz", "Jacobo Naya", "Nacho Mur"],
        yearsActive: "2011 - present"
    },
    {
        name: "Maga",
        albums: ["Maga (blanco)", "Maga (negro)", "Maga (rojo)", "A la hora del Sol", "SSatie contra Godzilla", "15º Aniversario", "Salto Horizontal"],
        members: ["Miguel Rivera", "Javier Vega", "Pablo Cabra", "César Díaz"],
        yearsActive: "2000 - present"
    },
    {
        name: "Crystal Fighters",
        albums: ["Star of Love", "Cave Rave", "Everything is my Family", "Gaia and Friends"],
        members: ["Sebastian Pringle", "Gilbert Vierich", "Graham Dickson", "Eleanor Fletcher", "Nila Raja"],
        yearsActive: "2007 - present"
    },
    {
        name: "Miss Caffeina",
        albums: ["Imposibilidad del Fenómeno", "De polvo y Flores", "Detroit", "Oh Long Johnson", "El año del Tigre"],
        members: ["Alberto Jiménez", "Sergio Sastre", "Antonio Poza"],
        yearsActive: "2006 - present"
    },
    {
        name: "Love of Lesbian",
        albums: ["Microscopic Movies", "Is It Fiction?", "Ungravity", "Maniobras de Escapismo", "Cuentos Chinos Para Niños del Japón", "1999 (o cómo generar incendios de nieve con una lupa enfocando a la Luna)", "La Noche Eterna. Los Días No Vividos", "El Poeta Halley", "El Gran Truco Final", "V.E.H.N (Viaje Épico Hacia la Nada"],
        members: ["Santi Balmes", "Julián Saldarriaga", "Jordi Roig", "Ricky Falkner", "Oriol Bonet"],
        yearsActive: "1997 - present"
    },
    {
        name: "Fuel Fandango",
        albums: ["Fuel Fandango", "Trece Lunas", "Aurora", "Origen"],
        members: ["Cristina Manjón", "Alejandro Acosta"],
        yearsActive: "2010 - present"
    },
    {
        name: "Izal",
        albums: ["Magia y Efectos Especiales", "Agujeros de Gusano", "Copacabana", "VIVO", "Autoterapia", "Hogar"],
        members: ["Mikel Izal", "Alejandro Jordá", "Emanuel Pérez 'Gato'", "Alberto Pérez", "Iván Mella"],
        yearsActive: "2010 - 2022"
    },
    {
        name: "Vetusta Morla",
        albums: ["Un Día en el Mundo", "Mapas", "La Deriva", "Mismo Sitio, Distinto Lugar", "Cable a Tierra"],
        members: ["Pucho", "David 'el Indio'", "Julio Benítez", "Jorge González", "Guillermo Galván", "Juanma Latorre"],
        yearsActive: "1998 - present"
    },
    {
        name: "Rufus T. Firefly",
        albums: ["My Synthetic Heart", "Ø", "Grunge", "Nueve", "Magnolia", "Loto", "El Largo Mañana"],
        members: ["Victor Cabezuelo", "Carlos Campos", "Julia Martín-Maestro", "Juan Feo", "Miguel de Lucas", "Marta Brandariz"],
        yearsActive: "2006 - present"
    },
    {
        name: "Veintiuno",
        albums: ["Nada Parecido", "Gourmet", "Corazonada"],
        yearsActive: "2015 - present"
    },
    {
        name: "Morgan",
        albums: ["North", "Air", "The River and the Stone"],
        members: ["Carolina de Juan", "Paco López", "David Schulthess", "Ekain Elorza"],
        yearsActive: "2012 - present"
    },
    {
        name: "Arde Bogotá",
        albums: ["La Noche"],
        yearsActive: "2019 - present"
    },
    {
        name: "Siloé",
        albums: ["La Verdad", "La Luz", "Metrópolis"],
        members: ["Fito Robles", "Xavi Road"],
        yearsActive: "2016 - present"
    },
    {
        name: "Carmen Boza",
        albums: ["La Mansión de los Espejos", "La Caja Negra"],
        members: ["Carmen Boza"],
        yearsActive: "2011 - present"
    },
    {
        name: "Sexy Zebras",
        albums: ["Volvamos a la Selva", "Hola, Somos los Putos Sexy Zebras", "La Polla", "Calle Liberación"],
        yearsActive: "2005 - present"
    },
    {
        name: "Cala Vento",
        albums: ["Cala Vento", "Fruto Panorama", "Balanceo"],
        members: ["Joan Delgado", "Aleix Turon"],
        yearsActive: "2016 - present"
    },
    {
        name: "Dorian",
        albums: ["10.000 Metrópolis", "El Futuro no es de Nadie", "La Ciudad Subterránea", "La Velocidad del Vacío", "Diez Años y un Día", "Justicia Universal", "Ritual"],
        members: ["Marc Gili", "Belly Hernández", "Bart Sanz", "Víctor López", "Lisandro Montes"],
        yearsActive: "2002 - present"
    },
    {
        name: "Viva Suecia",
        albums: ["La Fuerza Mayor", "Otros Principios Fundamentales", "El Milagro"],
        members: ["Rafa Val", "Jess Fabric", "Alberto Cantúa", "Fernando Campillo"],
        yearsActive: "2013 - present"
    },
    {
        name: "Iván Ferreiro",
        albums: ["Canciones Para el Tiempo y la Distancia", "Mentiroso Mentiroso", "Pícnic Extraterrestre", "Confesiones de un Artista de Mierda", "Val Miñor-Madrid", "Casa", "Cena Recalentada"],
        members: ["Iván Ferreiro"],
        yearsActive: "1991 - present"
    },
    {
        name: "Shinova",
        albums: ["Latidos", "La Ceremonia de la Confusión", "Ana y el Artista Temerario", "Volver", "Cartas de Navegación", "La Buena Suerte"],
        members: ["Gabriel de la Rosa", "Daniel del Valle", "Erlantz Prieto", "Ander Cabello", "Joshua Froufe"],
        yearsActive: "2008 - present"
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        const allBands = await Band.find();

        if(allBands.length) {
            console.log('Removing bands collection...');
            await Band.collection.drop();
        } else {
            console.log("There's no bands in database... adding bands")
        }
    })
    .catch(error => console.log('Error removing collection from database', error))
    .then(async () => {
        await Band.insertMany(initialBands);
        console.log('Bands added successfully...');
    })
    .catch(error => console.log('Error adding bands to database', error))
    .finally(() => mongoose.disconnect());