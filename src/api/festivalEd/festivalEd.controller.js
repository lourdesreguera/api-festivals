const FestivalEdition = require('./festivalEd.model');

const getAllFestivalEditions = async (req, res, next) => {
    try {
        const allFestivalEditions = await FestivalEdition.find().populate('bands');
        return res.status(200).json(allFestivalEditions);
    } catch (error) {
        return next(error);
    }
};

const postFestivalEdition = async (req, res, next) => {
    try {
        const newFestivalEdition = new FestivalEdition(req.body);
        const createdFestivalEdition = await newFestivalEdition.save();
        return res.status(201).json(createdFestivalEdition)
    } catch (error) {
        return next(error);
    }
};

const putFestivalEdition = async (req, res, next) => {
    try {
        const { id } = req.params;
        const festivalEdition = new FestivalEdition(req.body);
        festivalEdition._id = id; // cambiamos el id al nuevo objeto para actualizar el que genera por el suyo
        const updatedFestivalEdition = await FestivalEdition.findByIdAndUpdate(id, festivalEdition);
        return res.status(200).json(updatedFestivalEdition);
    } catch (error) {
       return next(error);
    }
};

const deleteFestivalEdition = async (req, res, next) => {
    try {
        const { id } = req.params;
        const deletedFestivalEdition = await FestivalEdition.findByIdAndDelete(id);
        return res.status(200).json(deletedFestivalEdition);
    } catch (error) {
        return next(error);
    }
};

module.exports = { getAllFestivalEditions, postFestivalEdition, putFestivalEdition, deleteFestivalEdition };