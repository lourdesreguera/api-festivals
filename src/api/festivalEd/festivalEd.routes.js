const express = require('express');
const { getAllFestivalEditions, postFestivalEdition, putFestivalEdition, deleteFestivalEdition } = require('./festivalEd.controller');

const router = express.Router();

router.get('/', getAllFestivalEditions);
router.post('/new', postFestivalEdition);
router.put('/edit/:id', putFestivalEdition);
router.delete('/delete/:id', deleteFestivalEdition);

module.exports = router;