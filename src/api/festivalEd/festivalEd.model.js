const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const festivalEditionSchema = new Schema(
    {
        festival: { type: String, required: true },
        date: { type: String, required: true },
        bands: [{ type: mongoose.Types.ObjectId, ref: 'Band' }]
    },
    {
        timestamps: true,
    }
);

const FestivalEdition =  mongoose.model('FestivalEdition', festivalEditionSchema);

module.exports = FestivalEdition;