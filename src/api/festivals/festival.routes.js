const express = require('express');
const { getAllFestivals, postFestival, putFestival, deleteFestival, getFestivalByName } = require('./festival.controller');

const router = express.Router();

router.get('/', getAllFestivals);
router.get('/name/:name', getFestivalByName)
router.post('/new', postFestival);
router.put('/edit/:id', putFestival);
router.delete('/delete/:id', deleteFestival);

module.exports = router;
