const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const festivalSchema = new Schema(
    {
        name: { type: String, required: true },
        location: { type: String },
        editions: [{ type: mongoose.Types.ObjectId, ref: 'FestivalEdition',  localField: 'editions',
        foreignField: 'bands' }]
    },
    {
        timestamps: true,
    },
);

const Festival = mongoose.model('Festival', festivalSchema);

module.exports = Festival;