const Festival = require('./festival.model');

const getAllFestivals = async (req, res, next) => {
    try {
        const allFestivals = await Festival.find().populate({
            path: "editions", 
            populate: {
               path: "bands" 
            }
         })
        return res.status(200).json(allFestivals);
    } catch (error) {
        return next(error);
    }
};

const getFestivalByName = async (req, res, next) => {
    try {
        const { name } = req.params;
        const festival = await Festival.find({name: name}).populate({
            path: "editions", 
            populate: {
               path: "bands" 
            }
         });
        if (festival.length) {
            return res.status(200).json(festival);
        } else {
            return res.status(404).json('Festival not found with this name');
        }
    } catch (error) {
        return next(error);
    }
};

const postFestival = async (req, res, next) => {
    try {
        const newFestival = new Festival(req.body);
        const createdFestival = await newFestival.save();
        return res.status(201).json(createdFestival)
    } catch (error) {
        return next(error);
    }
};

const putFestival = async (req, res, next) => {
    try {
        const { id } = req.params;
        const festival = new Festival(req.body);
        festival._id = id; // cambiamos el id al nuevo objeto para actualizar el que genera por el suyo
        const updatedFestival = await Festival.findByIdAndUpdate(id, festival);
        return res.status(200).json(updatedFestival);
    } catch (error) {
       return next(error);
    }
};

const deleteFestival = async (req, res, next) => {
    try {
        const { id } = req.params;
        const deletedFestival = await Festival.findByIdAndDelete(id);
        return res.status(200).json(deletedFestival);
    } catch (error) {
        return next(error);
    }
};

module.exports = { getAllFestivals, getFestivalByName, postFestival, putFestival, deleteFestival };