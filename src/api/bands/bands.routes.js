const express = require('express');
const { getAllBands, getBandById, getBandByName, postBand, putBand, deleteBand } = require('./bands.controller');

const router = express.Router();

router.get('/', getAllBands);
router.get('/:id', getBandById);
router.get('/name/:name', getBandByName);
router.post('/new', postBand);
router.put('/edit/:id', putBand);
router.delete('/delete/:id', deleteBand);

module.exports = router;