const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bandsSchema = new Schema(
    {
        name: { type: String, required: true },
        albums: [{ type: String, required: true }],
        members: [{ type: String }],
        yearsActive: { type: String }
    },
    {
        timestamps: true,
    }
);

const Band = mongoose.model('Band', bandsSchema);

module.exports = Band;