const Band = require('./bands.model');

const getAllBands = async (req, res, next) => {
    try {
        const allBands = await Band.find();
        return res.status(200).json(allBands);
    } catch (error) {
        return next(error);
    }
};

const getBandById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const band = await Band.findById(id);
        if (band) {
            return res.status(200).json(band);
        } else {
            return res.status(404).json('Band not found with this id');
        }
    } catch (error) {
        return next(error);
    }
};

const getBandByName = async (req, res, next) => {
    try {
        const { name } = req.params;
        const band = await Band.find({name: name});
        if (band.length) {
            return res.status(200).json(band);
        } else {
            return res.status(404).json('Band not found with this name');
        }
    } catch (error) {
        return next(error);
    }
};

const postBand = async (req, res, next) => {
    try {
        const newBand = new Band(req.body);
        const createdBand = await newBand.save();
        return res.status(201).json(createdBand);
    } catch (error) {
        return next(error);
    }
};

const putBand = async (req, res, next) => {
    try {
        const { id } = req.params;
        const band = new Band(req.body);
        band._id = id; // cambiamos el id al nuevo objeto para actualizar el que genera por el suyo
        const updatedBand = await Band.findByIdAndUpdate(id, band);
        return res.status(200).json(updatedBand);
    } catch (error) {
       return next(error);
    }
};

const deleteBand = async (req, res, next) => {
    try {
        const { id } = req.params;
        const deleteBand = await Band.findByIdAndDelete(id);
        return res.status(200).json(deleteBand);
    } catch (error) {
        return next(error);
    }
};

module.exports = { getAllBands, getBandById, getBandByName, postBand, putBand, deleteBand };